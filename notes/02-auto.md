## Chapter 02. `auto`声明

* 条目 5：优先使用`auto`声明而非显式的类型声明。

* 条目 6：当`auto`推导出的类型不是期望类型时应当显式指出类型。

`auto`类型声明很好用，但是也有一些陷阱。这一章的内容就是用来指导我们如何使用好`auto`类型声明，毕竟显式类型声明只是备选方案。

### 1. 优先使用`auto`

`auto`声明有如下有点：

1. `auto`声明可以避免忘记变量初始化。因为`auto`需要从初始值推导类型，不初始化当然没法推导，编译器自然会罢工。

```C++
int a1;        // 可以编译通过，编译器可能会给a1赋予初始值，如全局变量时初始化为0，局部变量将未定义
               // https://zh.cppreference.com/w/cpp/language/default_initialization
// auto a2;    // 编译失败，因为auto需要从值推导类型
auto a2 = 1;   // 正确，可以防止变量未初始化
```

2. `auto`可以简化复杂类型的声明。

```C++
std::vector<int> iVec{1, 2, 3};

std::vector<int>::iterator it1 = iVec.begin();  // 复杂
auto it2 = iVec.begin();                        // 简单
```

3. `auto`可以表示某些只有编译器知道的类型（闭包，如`lambda`表达式）

```C++
auto cmp1 = [](const int &a, const int &b) {
    return a > b;
};

// C++14起甚至连lambda表达式的形参都可以使用auto
auto cmp2 = [](const auto &a, const auto &b) {
    return a > b;
};
```

**注意！！**虽然这里也可以使用**`std::function`**来声明（可调用对象），但本质上`std::function`并不是其真正的类型。`std::function`是从C++11起标准库引入用以泛化函数指针概念的一个模板类。不同的是，函数指针只可以指向函数，`std::function`却可以指向任何可调用对象，如函数、`lambda`表达式、`bind`表达式或其他函数对象等一切可以像函数调用一样调用的东西。和函数声明一样，`std::function`声明时也需要指定可调用对象的类型，这通过指定`std::function`模板参数达到。比如如下函数声明：

```C++
bool(const std::unique_ptr<Widget>&, const std::unique_ptr<Widget>&);
```

使用`std::function`声明为：

```C++
std::function<bool(const std::unique_ptr<Widget>&,
                   const std::unique_ptr<Widget>&)> func;
```

于是，之前的`lambda`表达式可以声明为：

```C++
std::function<bool(const int&, const int&)> cmp =
    [](const int &a, const int &b) { return a > b; };
```

抛开`std::function`冗长的语法不谈，它和`auto`本质上还是不一样的。`auto`声明的对象拥有和闭包完全一致的类型（我们不知道，只有编译器知道），并且它只需要占用和闭包一样的内存。但是`std::function`声明的对象却是`std::function`模板对应该闭包类型的一个实例，它对应具体的声明占用的内存是固定的（？？），但是闭包需要的内存可能超过这个值，这样的话，`std::function`构造函数就会在堆上分配新的内存来存放闭包。这导致`std::function`对象比`auto`声明的对象占用更多的内存。并且由于`std::function`的实现对内联函数有限制，这又导致了通过`std::function`对象调用闭包比通过`auto`声明的对象调用更慢。简言之，`std::function`方法相比于`auto`**既慢又大**，并且它可能导致内存溢出错误（Out of Memory）。而且，`auto`写起来显然更简练。

4. `auto`声明可以避免由于我们不清楚确切类型而盲目使用显示声明导致的错误（作者成为**Type Shortcuts**问题，类型截断问题）。

比如：

```C++
unsigned sz1 = iVec.size();    // sz1 类型 unsigned
auto sz2 = iVec.size();        // sz2 类型推导为 std::vector<int>::size_type
// iVec.size() 返回类型为 std::vector<int>::size_type。
// 对于sz1，在32为系统下他们大小是一致的，但是64位系统下就不一样了。auto就不会出现这样的问题
```

在举一个例子，关于`std::unordered_map<std::string, int>`的`value_type`。为了直观显示结果，来定义一个帮助类。

```C++
// help class defined for demonstrate `type shortcuts`
// begin help
struct TypeShortcut {
    TypeShortcut(std::string s) : mString(std::move(s)) {}

    void echo() const {
        std::cout << mString << std::endl;
    }

    ~TypeShortcut() {
        mString = "oops! I have been destroyed";
    }

    std::string mString;
};

struct TypeShortcutHash {
    std::size_t operator()(const TypeShortcut &ts) const {
        return std::hash<std::string>{}(ts.mString);
    }
};

struct TypeShortcutEqual {
    bool operator()(const TypeShortcut &lts, const TypeShortcut &rts) const {
        return lts.mString == rts.mString;
    }
};
// end help

// define an unordered_map object
std::unordered_map<TypeShortcut, int, TypeShortcutHash, TypeShortcutEqual> typeShortcutMap{{{"Hello"}, 1}, {{"World"}, 2}};
```

看下面这段代码：

```C++
// use explicit type
const TypeShortcut *pTS1 = nullptr;
for (const std::pair<TypeShortcut, int> &ts : typeShortcutMap) {
    pTS1 = &(ts.first);
    std::cout << "e.g.1 - in:    ";
    // yes, output in map
    pTS1->echo();
}
std::cout << "e.g.1 - out:   ";
// "oops! I have been destroyed"
pTS1->echo();

// 输出结果
/*
e.g.1 - in:    World
e.g.1 - in:    Hello
e.g.1 - out:   oops! I have been destroyed
*/
```

奇怪，`ts`明明是引用，怎么出了循环就被析构了呢。原因就出在`std::unordered_map`的`value_type`上。它的`value_type`是[`std::pair<const Key, T>`](https://zh.cppreference.com/w/cpp/container/unordered_map)，这里也就是`std::pair<const TypeShortcut, int>`。循环中使用的是`std::pair<TypeShortcut, int>`，非`const`引用不能指向`const`变量，所以`ts`实际上指向的是一份临时拷贝，循环之后自然就GG了。说到底，这是因为我们在不清楚确切类型时使用了错误的显式类型声明。这里使用`auto`就没问题了。C++真是处处是坑啊。

```C++
// use auto
const TypeShortcut *pTS2 = nullptr;
for (const auto &ts : typeShortcutMap) {
    pTS2 = &(ts.first);
    std::cout << "e.g.2 - in:    ";
    // yes, output content in map
    pTS2->echo();
}
std::cout << "e.g.2 - out:   ";
// still output content in map
pTS2->echo();

// 输出结果
/*
e.g.2 - in:    World
e.g.2 - in:    Hello
e.g.2 - out:   Hello
*/
```

### `auto`缺点与陷阱

