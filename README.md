# Reading Notes of `Effective Modern C++`

#### Requirements:
- [Compiler must fully support C++ 14 (GCC 5.0 / Clang 3.4 / MSVC 19.10)](https://zh.cppreference.com/w/cpp/compiler_support)
- [`boost`](https://www.boost.org/users/download/)

#### Tests:
- in folder `codes/`, execute `./build all` for first time or after `CMakeLists.txt` changed, otherwise, `./build` is preferred
- tests in `codec/bin/`

#### Contents:
1. [Deducing Types](notes/01-deducing-types.md)
2. [`auto`](notes/02-auto.md)
3. Moving to Modern C++
4. Smart Pointers
5. Rvalue References, Move Semantics, and Perfect Forwarding
7. The Concurrency API
8. Tweaks