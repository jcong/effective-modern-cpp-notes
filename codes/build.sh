#!/bin/bash

if [ "$1" == "all" ]; then
    rm -rf CMakeFiles/
    rm -rf CMakeCache.txt
    rm -rf bin/

    cmake .
fi

make clean
make
