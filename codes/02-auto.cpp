/*
 * < Effective Modern C++ >
 * `auto`:
 *  item 5: Prefer `auto` to explicit type declarations
 *  item 6: Use the explicitly typed initializer idiom when `auto` deduces undesired types
 */

#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>

// help class defined for demonstrate `type shortcuts`
// begin help
struct TypeShortcut {
    TypeShortcut(std::string s) : mString(std::move(s)) {}

    void echo() const {
        std::cout << mString << std::endl;
    }

    ~TypeShortcut() {
        mString = "oops! I have been destroyed";
    }

    std::string mString;
};

struct TypeShortcutHash {
    std::size_t operator()(const TypeShortcut &ts) const {
        return std::hash<std::string>{}(ts.mString);
    }
};

struct TypeShortcutEqual {
    bool operator()(const TypeShortcut &lts, const TypeShortcut &rts) const {
        return lts.mString == rts.mString;
    }
};
// end help

int main() {
    // item 5.

    // 1. avoid uninitialized variables
    int a1;         // compiles ok, but al's value is indeterminate
                    // https://zh.cppreference.com/w/cpp/language/default_initialization
    // auto a2;     // compile failed, a2 must be initialized, because `auto` need to deduce type from initializer
    auto a2 = 0;    // fine, deduced type is int.

    // 2. simplify the complex type declaration
    std::vector<int> iVec{1, 2, 3};

    std::vector<int>::iterator it1 = iVec.begin();  // complex
    auto it2 = iVec.begin();                        // simple

    // 3. can represent some types only known to compiler
    // such as lambda expression
    auto add1 = [](const int &a, const int &b) {
        return a + b;
    };
    // even parameters can be declared using `auto` since C++14
    auto add2 = [](const auto &a, const auto &b) {
        return a + b;
    };
    // Although we can use std::function<int(int, int)> to declare add1, it's different type than lambda expression.
    // And it's generally bigger and slower than `auto` approach, and it may yield OOM exceptions

    // 4. type shortcuts issue
    // e.g. 1
    // iVec.size() is std::vector<int>::size_type
    unsigned sz1 = iVec.size();     // sz1's type is unsigned
    auto sz2 = iVec.size();         // sz2's type is std::vector<int>::size_type

    // e.g. 2
    std::unordered_map<TypeShortcut, int, TypeShortcutHash, TypeShortcutEqual>
        typeShortcutMap{{{"Hello"}, 1}, {{"World"}, 2}};

    // use explicit type
    const TypeShortcut *pTS1 = nullptr;
    for (const std::pair<TypeShortcut, int> &ts : typeShortcutMap) {
        pTS1 = &(ts.first);
        std::cout << "e.g.1 - in:    ";
        // yes, output in map
        pTS1->echo();
    }
    std::cout << "e.g.1 - out:   ";
    // "oops! I have been destroyed"
    pTS1->echo();

    // use auto
    const TypeShortcut *pTS2 = nullptr;
    for (const auto &ts : typeShortcutMap) {
        pTS2 = &(ts.first);
        std::cout << "e.g.2 - in:    ";
        // yes, output content in map
        pTS2->echo();
    }
    std::cout << "e.g.2 - out:   ";
    // still output content in map
    pTS2->echo();

    // item 6.
    auto echoBoolVector = [](const std::vector<bool> &vec) {
        for (const auto &v : vec) {
            std::cout << std::boolalpha << v << ", ";
        }
        std::cout << std::endl;
    };

    std::vector<bool> boolVec1{false, false, false};
    std::vector<bool> boolVec2{false, false, false};

    for (auto b : boolVec1) {
        b = true;
    }
    // true, true, true,
    echoBoolVector(boolVec1);

    for (bool b : boolVec2) {
        b = true;
    }
    // false, false, false
    echoBoolVector(boolVec2);
}

