/*
 * < Effective Modern C++ >
 * Deducing Types:
 *  item 1: Understand template type deduction
 *  item 2: Understand `auto` type deduction
 *  item 3: Understand `decltype`
 *  item 4: Know how to view deduced types
 */

#include <iostream>
#include <vector>
#include <string>

#include <boost/type_index.hpp>

/*
 * Help function
 */

#define ECHO_TYPE(T, U)                                             \
{                                                                   \
    using boost::typeindex::type_id_with_cvr;                       \
    std::cout.width(10), std::cout.setf(std::ios::left);            \
    std::cout << #U << " = "                                        \
              << type_id_with_cvr<T>().pretty_name()                \
              << std::endl;                                         \
}                                                                   \

#define ECHO_T_TYPE(T)          ECHO_TYPE(T, T)
#define ECHO_PARAM_TYPE(param)  ECHO_TYPE(decltype(param), param)
#define ECHO()                                                      \
{                                                                   \
    std::cout << std::string(30, '-') << std::endl;                 \
    ECHO_T_TYPE(T);                                                 \
    ECHO_PARAM_TYPE(param);                                         \
}                                                                   \

/*
 * Template Deduction
 */

// Case 1. paramType is lvalue reference or pointer
template <typename T>
void f1(T &param) {
    ECHO();
}

// Case 2. paramType is universal reference
template <typename T>
void f2(T &&param) {
    ECHO();
}

// Case 3. paramType is passed-by-value
template <typename T>
void f3(T param) {
    ECHO();
}

// get array length
template <typename T, std::size_t N>
constexpr std::size_t getArrayLength(const T (&)[N]) {
    return N;
}

// function to deduce
void funcToDeduce(int) { return; }


/*
 * `auto` Deduction
 */
template <typename T>
void f4(std::initializer_list<T> &&param) {
    ECHO();
}

// auto generateInitList() {
//     return {1};              // Compile Error
// }


/*
 * `decltype` deduction
 */
// for C++ 11 with trailing return type
template <typename Container, typename Index>
auto getIndexAt1(Container &c, Index i) -> decltype(c[i]) {
    return c[i];
}

// Since C++14, but reference info is lost
template <typename Container, typename Index>
auto getIndexAt2(Container &c, Index i) {
    return c[i];
}

// decltype(auto)
template <typename Container, typename Index>
decltype(auto) getIndexAt3(Container c, Index i) {
    return c[i];
}

// perfect forwarding
template <typename Container, typename Index>
decltype(auto) getIndexAt4(Container &&c, Index i) {
    return std::forward<Container>(c)[i];
}

int main() {
    int v = 10;             // int
    int &rv = v;            // int&
    const int cv = v;       // const int
    const int &crv = v;     // const int&


    std::cout << "***   Template Deduction  ***" << std::endl;
    std::cout << "Case. 1" << std::endl;
    f1(v);                  // T is int, paramType is int&
    f1(rv);                 // T is int, paramType is int&
    f1(cv);                 // T is const int, paramType is const int&
    f1(crv);                // T is const int, paramType is const int&

    std::cout << "Case. 2" << std::endl;
    f2(v);                  // T and paramType are both int&
    f2(rv);                 // T and paramType are both int&
    f2(cv);                 // T and paramType are both const int&
    f2(crv);                // T and paramType are both const int&
    f2(std::move(v));       // T is int, paramType is int&&
    f2(std::move(rv));      // T is int, paramType is int&&
    f2(std::move(cv));      // T is const int, paramType is int&&
    f2(std::move(crv));     // T is const int, paramType is int&&

    std::cout << "Case. 3" << std::endl;
    f3(cv);                 // both T and paramType are int
    f3(crv);                // both T and paramType are int
    const char *str1{"something"};  // str1 is const char*
    // !note! ISO C++ forbids `const char*` to `char *`
    char *const str2{"something"};  // str2 is char* const
    f3(str1);               // both T and paramType is const char*
    f3(str2);               // both T and paramType is char*

    std::cout << "Array argument" << std::endl;
    int a[] = {1, 2, 3};    // int[3]
    f1(a);                  // T is int[3], paramType is int(&)[3]
    f2(a);                  // both T is int(&)[3]
    f3(a);                  // both T and paramType are int*

    std::cout << "Get array length" << std::endl;
    std::cout << "  use template deduction: " << getArrayLength(a) << std::endl;
    std::cout << "  use sizeof(): " << sizeof(a) / sizeof(a[0]) << std::endl;
    std::cout << "  use std::extent: " << std::extent<decltype(a)>::value << std::endl;

    std::cout << "Function argument" << std::endl;
    // declaration: void funcToDeduce(int);    // type: void(int)
    f1(funcToDeduce);       // T is void(int), paramType is void(&)(int)
    f2(funcToDeduce);       // both T and paramType is void(&)(int)
    f3(funcToDeduce);       // both T and paramType is void(*)(int）


    std::cout << "***    `auto` Deduction  ***" << std::endl;
    auto x1 = 1;            // int
    auto x2(1);             // int
    auto x3 = {1};          // std::initializer_list<int>
    auto x4{1};             // int since C++17, std::initializer_list<int> before C++17
    auto x5 = {1, 2};       // obviously std::initializer_list<int>
    // auto x6{1, 2};       // FORBIDDEN
    ECHO_PARAM_TYPE(x1);
    ECHO_PARAM_TYPE(x2);
    ECHO_PARAM_TYPE(x3);
    ECHO_PARAM_TYPE(x4);
    ECHO_PARAM_TYPE(x5);
    f4({1});                // T is int, paramType is std::initializer_list<int>&&

    // std::vector<int> vec;
    // auto resetVector = [&](const auto &v) {
    //     vec = v;
    // };
    // resetVector({1});               // Compile error

    std::cout << "*** `decltype` Deduction ***" << std::endl;
    // ECHO_T_TYPE() and ECHO_PARAM_TYPE() macro are using decltype() to get precious type

    std::vector<int> vec{1, 2, 3, 4, 5};
    ECHO_PARAM_TYPE(getIndexAt1(vec, 1));   // int&
    ECHO_PARAM_TYPE(getIndexAt2(vec, 1));   // int
    ECHO_PARAM_TYPE(getIndexAt3(vec, 1));   // int&
    ECHO_PARAM_TYPE(getIndexAt4(vec, 1));   // int&
}
